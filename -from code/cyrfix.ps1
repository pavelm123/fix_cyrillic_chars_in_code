#get-childitem -recurse | select Extension | group extension | select count, name | sort count -desc| out-file -FilePath c-code-platform-ext
$ext = get-content c-code-platform-ext 
echo "processed extensions $($ext.Trim() -join ",")"

$cyrcharregex = [regex]"[������������]"
[string[]]$Excludes = @('*archive*', '*archival*')

get-childitem -recurse -Include $ext.Trim() -exclude $Excludes | select-string -pattern $cyrcharregex -Encoding default | select path, linenumber, matches | fl *