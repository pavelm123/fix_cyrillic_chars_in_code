param($searchString)

#read .gitignore from curent directory and exclude all comment lines
$gitignore = get-content ".gitignore" | select-string -pattern "^\#" -notmatch | where {$_.Line.Trim() -ne ""}
$exclude = $gitignore -join ","
$exclude = "bin/,Bin/,Obj/,obj/,TestResults,*.suo,*.user,*.sln.docstates,debug/,release/,x64/,*_i.c,*_p.c,*.ilk,*.meta,*.obj,*.pch,*.pdb,*.pgc,*.pgd,*.rsp,*.sbr,*.tlb,*.tli,*.tlh,*.tmp,*.vspscc,*.vssscc,.builds,.vs/,ipch/,*.aps,*.ncb,*.opensdf,*.sdf,*.psess,*.vsp,*.gpState,_ReSharper*,*.ncrunch*,.*crunch*.local.xml,express,DocProject/buildhelp/,DocProject/Help/*.HxT,DocProject/Help/*.HxC,DocProject/Help/*.hhc,DocProject/Help/*.hhk,DocProject/Help/*.hhp,DocProject/Help/Html2,DocProject/Help/html,publish,*.Publish.xml,bin,obj,sql,TestResults,testresult*,*.Cache,ClientBin,stylecop.*,~$*,*.dbmdl,Generated_Code #added for RIA/Silverlight projects,_UpgradeReport_Files/,Backup*/,UpgradeLog*.XML,*.userprefs,*.usertasks,*.pidb,*.resources,test-results/,*.suo,*.user,*.sln.docstates,debug*/,release/,testresult,buildlog.*,*_i.c,*_p.c,*.ilk,*.meta,*.obj,*.pch,*.pdb,*.pgc,*.pgd,*.rsp,*.sbr,*.tlb,*.tli,*.tlh,*.tmp,*.vspscc,*.vssscc,.builds,*.pidb,*.log,*.scc,ipch/,*.aps,*.ncb,*.opensdf,*.sdf,*.psess,*.vsp,*.gpState,_ReSharper*/,*.resharper,*.ncrunch*,.*crunch*.local.xml,express,DocProject/buildhelp/,DocProject/Help/*.HxT,DocProject/Help/*.HxC,DocProject/Help/*.hhc,DocProject/Help/*.hhk,DocProject/Help/*.hhp,DocProject/Help/Html2,DocProject/Help/html,publish,*.Publish.xml,[Bb]in,[Oo]bj,sql,TestResults,[Tt]est[Rr]esult*,*.Cache,ClientBin,[Ss]tyle[Cc]op.*,~$*,*.dbmdl,*.[Pp]ublish.xml,Generated_Code #added for RIA/Silverlight projects,_UpgradeReport_Files/,Backup*/,UpgradeLog*.XML,.DS_Store,POS/EverServ/build/OLOEverServService.wixobj,CallCenter/CallCenter/App_Data/Errors,*.nupkg,**/packages/*,!**/packages/build/,!**/packages/repositories.config/,node_modules,**/Content/bundles/*,rev-manifest.json"

# look here for .gitignore syntax
# http://stackoverflow.com/questions/8527597/how-do-i-ignore-files-in-a-directory-in-git

#get-childitem -recurse | select Extension | group extension | select count, name | sort count -desc| out-file -FilePath c-code-platform-ext
#$ext = get-content c-code-platform-ext 
#echo "processed extensions $($ext.Trim() -join ",")"

#$cyrcharregex = [regex]"[������������]"
#[string[]]$Excludes = @('*archive*', '*archival*')

#$exclude = @('*.cs', '*.csproj', '*.pdb', 'obj')

get-childitem -recurse -exclude $Excludes | select-string $searchString -Encoding default | select path, linenumber, line, matches | fl *